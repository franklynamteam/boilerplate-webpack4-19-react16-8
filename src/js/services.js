import 'bootstrap/scss/bootstrap.scss';
//import 'bootstrap/dist/css/bootstrap.css';
import React from "react";
import ReactDOM from "react-dom";
import $ from 'jquery';
import styles from "../less/styles.less";
import NavbarComponent from "../components/NavbarComponent";
import FooterComponent from "../components/FooterComponent";

ReactDOM.render(<NavbarComponent id="my-navbar" />, $('#header')[0]);
ReactDOM.render(<FooterComponent/>, $('#footer')[0]);

$(document).ready(function () {
	$('.hide-at-start').removeClass('hide-at-start');
	$('#loading').stop().fadeOut();
});
