import React from "react";
import styles from "./FooterComponent.less";
import SocialMediaLinksComponent from "../components/SocialMediaLinksComponent";

export default class FooterComponent extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
    <div ref={ div => { this.myFooter = div; }} class={styles.footer}>
        <div className={styles.sitemap}>
          <ul>
            <li><a href={window.location.origin.toString()}>home</a></li>
            <li><a href={window.location.origin.toString() + '/#about'}>about</a></li>
          </ul>
        </div>
        <SocialMediaLinksComponent />
        <div class={styles.legal}>
          <p>© Frank Lynam 2019</p>
        </div>
    </div>
    );
  }
}
