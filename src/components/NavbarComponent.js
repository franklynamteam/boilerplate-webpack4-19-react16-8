import React from "react";
import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/scss/bootstrap.scss';
//import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import styles from "./NavbarComponent.less";

export default class NavbarComponent extends React.Component {
  constructor(props) {
    var _id = props.id;
    $(window).unbind('resize.NavbarComponent').on('resize.NavbarComponent', function() {
      if ($('#' + _id + ' .' + styles['navbar-collapse']).hasClass('show')) {
        $('#' + _id + ' .' + styles['navbar-collapse']).removeClass(styles.show + ' show');
        $('#' + _id + ' .' + styles['navbar-overlay']).stop().fadeOut();
      }
    });
    super();
  }

  render() {
    var _id = this.props.id;
    function onToggleButtonClick(e) {
      $('#' + _id + ' .' + styles['navbar-collapse']).toggleClass(styles.show);
      if ($('#' + _id + ' .' + styles['navbar-collapse']).hasClass(styles.show)) {
        $('#' + _id + ' .' + styles['navbar-overlay']).show();
      } else {
        $('#' + _id + ' .' + styles['navbar-overlay']).stop().fadeOut();
      }
    }

    return (
      <div id={_id} className={styles['navbar-container']}>
        <div className={styles['navbar-overlay']}></div>
        <nav className={[styles.navbar, "navbar navbar-expand-lg"].join(' ')}>
          <button className={[styles['navbar-toggler'], "navbar-toggler"].join(' ')} type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation" onClick={onToggleButtonClick}>
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className={[styles['navbar-collapse'], "collapse navbar-collapse"].join(' ')} id="navbarNavAltMarkup">
            <div className="navbar-nav">
              <a className={[styles['nav-item'], "nav-item nav-link active"].join(' ')} href="#">Home <span className="sr-only">(current)</span></a>
              <a className={[styles['nav-item'], "nav-item nav-link"].join(' ')} href={window.location.origin.toString() + '/#about'}>About</a>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}
