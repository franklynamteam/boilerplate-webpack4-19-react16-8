import React from "react";
import styles from "./SocialMediaLinksComponent.less";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTwitter, faBehanceSquare, faInstagram, faLinkedin } from '@fortawesome/free-brands-svg-icons'

export default class SocialMediaLinksComponent extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
    <div className={styles.socialmedia}>
      <ul className={styles.contents}>
        <li><a href="https://twitter.com/flynam" target="_blank"><FontAwesomeIcon icon={faTwitter} /></a></li>
        <li><a href="./" target="_blank"><FontAwesomeIcon icon={faBehanceSquare} /></a></li>
        <li><a href="https://www.instagram.com/flynam/" target="_blank"><FontAwesomeIcon icon={faInstagram} /></a></li>
        <li><a href="https://www.linkedin.com/in/frank-lynam" target="_blank"><FontAwesomeIcon icon={faLinkedin} /></a></li>
      </ul>
    </div>
    );
  }
}
