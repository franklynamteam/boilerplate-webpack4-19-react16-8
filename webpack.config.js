const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
	mode: 'development',
	//mode: 'production',
	entry: {
		app: './src/js/index.js',
		services: './src/js/services.js'
	},
	devtool: 'inline-source-map',
	devServer: {
		contentBase: './dist'
	},
	module: {
		rules: [
		{
			test: /\.js$/,
			exclude: /(node_modules|bower_components)/,
			use: {
                loader: 'babel-loader'
            }
		},
		//component css - encapsulate classnames
		{
        	test: /src\/components\/.+\.less$/,
        	use: [
			{
				loader: "style-loader"
			},
			{
				loader: "css-loader",
				options: {
					sourceMap: true,
					modules: true,
					localIdentName: "[local]___[hash:base64:5]"
				}
			},
			{
				loader: "less-loader"
			}

        	]
		},
		//global css - do not add suffixes to class names
		{
        	test: /src\/less\/.+\.less$/,
        	use: [
			{
				loader: "style-loader"
			},
			{
				loader: "css-loader",
				options: {
					sourceMap: true,
					modules: 'global',
					//localIdentName: "[local]___[hash:base64:5]"
				}
			},
			{
				loader: "less-loader"
			}

        	]
		},
		{
			test: /\.(woff|woff2|eot|ttf|otf)$/,
			use: [
				'file-loader'
			]
		},
		{
      		test: /\.(jpe?g|png|gif|woff|woff2|eot|ttf|svg)(\?[a-z0-9=.]+)?$/,
			use: [
				'url-loader?limit=100000'
			]
		},
		//SCSS - for Bootstrap 4
      	{
			test: /\.(scss)$/,
			use: [
			{
				// Adds CSS to the DOM by injecting a `<style>` tag
				loader: 'style-loader'
			},
			{
				// Interprets `@import` and `url()` like `import/require()` and will resolve them
				loader: 'css-loader'
			},
			{
				// Loader for webpack to process CSS with PostCSS
				loader: 'postcss-loader',
				options: {
					plugins: function () {
						return [
						require('autoprefixer')
						];
					}
				}
			},
			{
				// Loads a SASS/SCSS file and compiles it to CSS
				loader: 'sass-loader'
			}
        	]
      	}		
		]
	},
	plugins: [
		new CleanWebpackPlugin(['dist']),
		new HtmlWebpackPlugin({
    		template: "./src/index.html",
    		filename: "./index.html",
    		chunks: ['app']
  		}),
		new HtmlWebpackPlugin({
    		template: "./src/services.html",
    		filename: "./services.html",
    		chunks: ['services']
  		})
	],
	output: {
		filename: '[name].bundle.js',
		path: path.resolve(__dirname, 'dist')
	}
};