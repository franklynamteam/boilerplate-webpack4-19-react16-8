# Boilerplate code for webpack React web app

## NPM

Have a look at package.json for a list of the Node libraries used by the project.

Install as follows:

	npm install

## Run the Dev Server (webpack-dev-server)

	npm start

## Build the Distribution folder

	webpack

